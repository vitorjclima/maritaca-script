#!/bin/bash
#
# Author: Vitor Lima
# Email:  vitor.lima@unifesp.br

#Pré-Requisito: Ter o git instalado
#sudo apt-get install -y git

################################## VARIABLES ##################################
JAVA_REPOSITORY="ppa:webupd8team/java"
FFMPEG_REPOSITORY="ppa:mc3man/trusty-media"
CASSANDRA_REPOSITORY="http://www.eu.apache.org/dist/cassandra/1.2.19"
TOMCAT_REPOSITORY="http://www.eu.apache.org/dist/tomcat/tomcat-7/v7.0.59/bin"
SOLR_REPOSITORY="http://archive.apache.org/dist/lucene/solr/4.2.0"
################################## FUNCTIONS ################################## 

add_repositorio_local(){
	sudo cp -r -p apt.conf /etc/apt/
}


criar_varAmbiente(){
	export MARITACA_CONFIG_PATH=/opt/configuration.properties
}



cloneProject(){
	cd	
	git clone http://git.code.sf.net/p/maritaca/code maritaca-code
}

buildProject(){
	cd 
	cd maritaca-code/server/
	mvn clean install -Dmaven.test.skip=true
}

add_repositorios(){
	sudo add-apt-repository -y $JAVA_REPOSITORY
	sudo add-apt-repository -y $FFMPEG_REPOSITORY
}


download_pacotes(){
	cd

	if [ -e "cassandra.tgz" ] ; then
		echo "Cassandra Já Baixado"
	else
		wget -O cassandra.tgz $CASSANDRA_REPOSITORY/apache-cassandra-1.2.19-bin.tar.gz
	fi

	if [ -e "tomcat7.tgz" ] ; then
		echo "Tomcat Já Baixado"
	else	
		wget -O tomcat7.tgz $TOMCAT_REPOSITORY/apache-tomcat-7.0.59.tar.gz
	fi

	if [ -e "solr.tgz" ] ; then
		echo "Solr Já Baixado"
	else
		wget -O solr.tgz $SOLR_REPOSITORY/solr-4.2.0.tgz
	fi

	if [ -e "android.tgz" ] ; then
		echo "Android Já Baixado"
	else
		wget -O android.tgz http://dl.google.com/android/android-sdk_r22.3-linux.tgz
	fi

}


extrair_pacotes(){
	cd
	tar zxvf cassandra.tgz
	tar zxvf tomcat7.tgz
	tar zxvf solr.tgz
	tar zxvf android.tgz
}


renomear_pastas_pacotes(){
	cd
	mv apache-cassandra-1.2.19/ cassandra
	mv apache-tomcat-7.0.59/ tomcat
	mv solr-4.2.0/ solr42
	mv android-sdk-linux/ androidSDK
}


copiar_pastas_pacotes(){
	cd

	#ARQUIVOS DE CONFIGURAÇÃO DO SOLR
	cp -r tomcat/ solr42/
	mkdir -p solr42/solr/data
	mkdir -p solr42/tomcat/conf/Catalina/localhost
	cp maritaca-code/doc/scripts/solr.xml solr42/tomcat/conf/Catalina/localhost/solr.xml
	cp solr42/dist/solr-4.2.0.war solr42/solr/solr.war
	cp solr42/example/* solr42/solr/data/.

	#ARQUIVO - VARIAVEIS DE AMBIENTE
	sudo cp -r -p maritaca-code/doc/scripts/configuration.properties /opt/

	#COPIA DO maritaca.war gerado após build de projeto
	sudo cp -r -p maritaca-code/server/web/target/maritaca.war tomcat/webapps/

	#DIRETÓROS HADOOP
	mkdir -p hadoop_mounted/user/maritaca/{apk,audio,picture,video}

	#DIRETÓRIO APPS
	mkdir -p maritaca-code/client/apps
	
	#COPIA DOS ARQUIVOS COM SUAS RESPECTIVAS PERMISSÕES PARA A PASTA /OPT
	sudo cp -r -p tomcat/ /opt/
	sudo cp -r -p solr42/ /opt/
	sudo cp -r -p cassandra/ /opt/
	sudo cp -r -p hadoop_mounted/* /var/www/
	sudo cp -r -p androidSDK /opt/
}





install_softwares(){
	sudo apt-get install -y oracle-jdk7-installer
	#sudo apt-get install -y openjdk-7-jdk	
	sudo apt-get install -y maven
	sudo apt-get install -y ant
	sudo apt-get install -y xvfb
	sudo apt-get install -y lynx
	sudo apt-get install -y rabbitmq-server
	sudo apt-get install -y ffmpeg
	#sudo apt-get install -y gstreamer0.10-ffmpeg
	#sudo apt-get install -y ia32-libs
	sudo apt-get install -y apache2	
}

atualizar_repositorios(){
	sudo apt-get update
}

parar_servicos(){
	sudo service apache2 stop
	sudo /opt/cassandra/bin/stop-server
	sudo /opt/solr42/tomcat/bin/shutdown.sh
	sudo /opt/tomcat/bin/shutdown.sh
}

iniciar_servicos(){
	sudo service apache2 start
	sudo /opt/cassandra/bin/cassandra
	sudo /opt/solr42/tomcat/bin/startup.sh
	sudo /opt/tomcat/bin/startup.sh -DMARITACA_CONFIG_PATH

}

configurate_cassandra(){
	cd
	sudo /opt/cassandra/bin/cassandra	
	/opt/cassandra/bin/cassandra-cli -h localhost -port 9160 -f maritaca-code/doc/scripts/cassandra_keyspace.txt
	sudo /opt/cassandra/bin/stop-server
}

configurate_rabbitMQ(){
	sudo rabbitmqctl add_user maritaca maritaca
	sudo rabbitmqctl add_vhost Maritaca
	sudo rabbitmqctl set_permissions -p Maritaca maritaca ".*" ".*" ".*"
}

configurate_solr42(){
	perl -i -pe 's/<Server port="8005" shutdown="SHUTDOWN">/<Server port="8105" shutdown="SHUTDOWN">/' /opt/solr42/tomcat/conf/server.xml
	perl -i -pe 's/<Connector port="8080" protocol="HTTP/<Connector port="8983" protocol="HTTP/' /opt/solr42/tomcat/conf/server.xml
	perl -i -pe 's/<Connector port="8009" protocol="AJP/<Connector port="8109" protocol="AJP/' /opt/solr42/tomcat/conf/server.xml
	#perl -i -pe 's//<dataDir>${solr.data.dir:/opt/solr42/solr/data}</dataDir>/' /opt/solr42/solr/data/collection1/conf/solrconfig.xml
}

configurate_tomcat(){
	perl -i -pe 's/Connector port="8080" protocol="HTTP\/1.1"\n               connectionTimeout="20000"\n               redirectPort="8443"/Connector port="8080" protocol="HTTP\/1.1"\n               connectionTimeout="20000"\n               redirectPort="8443" URIEncoding="UTF-8"/' /opt/tomcat/conf/server.xml

}

configurate_androidSDK(){
	cd /opt/
	androidSDK/tools/android list sdk
	androidSDK/tools/android update sdk --no-ui
}

##################################   CODE    ################################## 

add_repositorio_local
add_repositorios
atualizar_repositorios
cloneProject
criar_varAmbiente
install_softwares
buildProject
download_pacotes
extrair_pacotes
renomear_pastas_pacotes
copiar_pastas_pacotes


configurate_cassandra
configurate_rabbitMQ
configurate_solr42
configurate_androidSDK

#
#download_pacotes
#atualizar_repositorios
#add_softwares

