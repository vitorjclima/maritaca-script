#!/bin/bash
#
# Author: Vitor Lima
# Email:  vitor.lima@unifesp.br

################################## VARIABLES ##################################
JAVA_REPOSITORY="ppa:webupd8team/java"
FFMPEG_REPOSITORY="ppa:mc3man/trusty-media"
CASSANDRA_REPOSITORY="http://www.eu.apache.org/dist/cassandra/1.2.19"
TOMCAT_REPOSITORY="http://www.eu.apache.org/dist/tomcat/tomcat-7/v7.0.59/bin"
SOLR_REPOSITORY="http://archive.apache.org/dist/lucene/solr/4.2.0"
################################## FUNCTIONS ################################## 

add_repositorio_local(){
	cp -r -p apt.conf /etc/apt/
}

add_dependencias(){
	apt-get install -y python-software-properties
	apt-get install -y curl
}

cloneProject(){
	cd	
	git clone http://git.code.sf.net/p/maritaca/code maritaca-code
	sudo cp -r -p maritaca-code/ /opt/
}

buildProject(){
	cd maritaca
	mvn clean install
}

add_repositorios(){
	add-apt-repository -y $JAVA_REPOSITORY
	add-apt-repository -y $FFMPEG_REPOSITORY
}


download_pacotes(){
	cd

	if [ -e "cassandra.tgz" ] ; then
		echo "Cassandra Já Baixado"
	else
		wget -O cassandra.tgz $CASSANDRA_REPOSITORY/apache-cassandra-1.2.19-bin.tar.gz
	fi

	if [ -e "tomcat7.tgz" ] ; then
		echo "Tomcat Já Baixado"
	else	
		wget -O tomcat7.tgz $TOMCAT_REPOSITORY/apache-tomcat-7.0.59.tar.gz
	fi

	if [ -e "solr.tgz" ] ; then
		echo "Solr Já Baixado"
	else
		wget -O solr.tgz $SOLR_REPOSITORY/solr-4.2.0.tgz
	fi

}


extrair_pacotes(){
	cd
	tar zxvf cassandra.tgz
	tar zxvf tomcat7.tgz
	tar zxvf solr.tgz
}


renomear_pastas_pacotes(){
	cd
	mv apache-cassandra-1.2.19/ cassandra
	mv apache-tomcat-7.0.59/ tomcat
	mv solr-4.2.0/ solr42
}


copiar_pastas_pacotes(){
	cd
	cp -r -p tomcat/ solr42/	
	sudo cp -r -p maritaca-code/ /opt/
	sudo cp -r -p tomcat/ /opt/
	sudo cp -r -p solr42/ /opt/
	sudo cp -r -p cassandra/ /opt/
}

configurar_tomcat_solr42(){
	perl -i -pe 's/<Server port="8005" shutdown="SHUTDOWN">/<Server port="8105" shutdown="SHUTDOWN">/' /opt/solr42/tomcat/conf/server.xml
	perl -i -pe 's/<Connector port="8080" protocol="HTTP/1.1" connectionTimeout="20000" redirectPort="8443"/<Connector port="8983" protocol="HTTP/1.1" connectionTimeout="20000" redirectPort="8443"/' /opt/solr42/tomcat/conf/server.xml
	perl -i -pe 's/<Connector port="8009" protocol="AJP/1.3" redirectPort="8443"/<Connector port="8109" protocol="AJP/1.3" redirectPort="8443"/' /opt/solr42/tomcat/conf/server.xml

}



install_softwares(){
	#apt-get install -y oracle-jdk7-installer
	apt-get install -y openjdk-7-jdk	
	apt-get install -y git
	apt-get install -y maven
	apt-get install -y ant
	apt-get install -y xvfb
	apt-get install -y lynx
	apt-get install -y rabbitmq-server
	apt-get install -y ffmpeg
	#apt-get install -y gstreamer0.10-ffmpeg
	#apt-get install -y ia32-libs
	apt-get install -y apache2
		
}

atualizar_repositorios(){
	apt-get update
}

diretorios_hadoop(){
	cd /var/www
	mkdir -p hadoop_mounted/user/maritaca/{apk,audio,picture,video}
	chown -R maritaca:maritaca hadoop_mounted/


}


parar_servicos(){
	/opt/cassandra/bin/stop-server


}

iniciar_servicos(){
	/opt/cassandra/bin/cassandra

}

configurate_cassandra(){
	service cassandra status
	cassandra-cli -h localhost -port 9160 -f cassandra_keyspace.txt
}

configurate_rabbitMQ(){

	rabbitmqctl add_user maritaca maritaca
	rabbitmqctl add_vhost Maritaca
	rabbitmqctl set_permissions -p Maritaca maritaca ".*" ".*" ".*"

}

##################################   CODE    ################################## 

add_repositorio_local
add_repositorios
atualizar_repositorios
install_softwares
download_pacotes
extrair_pacotes
renomear_pastas_pacotes
#copiar_pastas_pacotes

#cloneProject
#download_pacotes
#atualizar_repositorios
#add_softwares
