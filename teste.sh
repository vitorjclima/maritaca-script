#!/bin/bash
#
# Author: Vitor Lima
# Email:  vitor.lima@unifesp.br

#Pré-Requisito: Ter o git instalado
#sudo apt-get install -y git

perl -i -pe 's/Connector port="8080" protocol="HTTP\/1.1"\n               connectionTimeout="20000"\n               redirectPort="8443"/Connector port="8080" protocol="HTTP\/1.1"\n               connectionTimeout="20000"\n               redirectPort="8443" URIEncoding="UTF-8"/' /home/vitor/tomcat/conf/server.xml
